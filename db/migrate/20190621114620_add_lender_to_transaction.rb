class AddLenderToTransaction < ActiveRecord::Migration[5.1]
  def change
    add_reference :transactions, :lender, foreign_key: true
  end
end
