class CreateLoans < ActiveRecord::Migration[5.1]
  def change
    create_table :loans do |t|
      t.belongs_to :debtor, foreign_key: true
      t.float :sum
      t.integer :percentage
      t.string :lond_type
      t.integer :number_of_month
      t.integer :static_return_value

      t.timestamps
    end
  end
end
