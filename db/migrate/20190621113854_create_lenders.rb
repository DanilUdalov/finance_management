class CreateLenders < ActiveRecord::Migration[5.1]
  def change
    create_table :lenders do |t|
      t.references :lenderable, polymorphic: true, index: true

      t.timestamps
    end
  end
end
