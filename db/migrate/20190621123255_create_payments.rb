class CreatePayments < ActiveRecord::Migration[5.1]
  def change
    create_table :payments do |t|
      t.integer :sum
      t.text :description
      t.belongs_to :debtor, foreign_key: true
      t.string :state
      t.string :paid_at

      t.timestamps
    end
  end
end
