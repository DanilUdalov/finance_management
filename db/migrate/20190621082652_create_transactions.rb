class CreateTransactions < ActiveRecord::Migration[5.1]
  def change
    create_table :transactions do |t|
      t.integer :amount_in_cents
      t.integer :debtor_id

      t.timestamps
    end
  end
end
