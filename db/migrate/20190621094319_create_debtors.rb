class CreateDebtors < ActiveRecord::Migration[5.1]
  def change
    create_table :debtors do |t|
       t.references :debtorable, polymorphic: true, index: true

      t.timestamps
    end
  end
end
