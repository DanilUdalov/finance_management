Rails.application.routes.draw do
  devise_for :users
  root to: 'home#index'

  namespace :admin do
    root to: 'users#index'
    resources :debtors
    resources :lenders
    resources :users, except: [:show]
    resources :organizations, except: [:show]
    resources :payments
  end
end
