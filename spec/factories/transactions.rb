FactoryBot.define do
  factory :transaction do
    amount_in_cents { 1 }
    debtor_id { 1 }
  end
end
