FactoryBot.define do
  factory :payment do
    sum { 1 }
    description { "MyText" }
    debtor { nil }
    state { "MyString" }
    paid_at { "MyString" }
  end
end
