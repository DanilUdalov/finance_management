FactoryBot.define do
  factory :lond do
    debtor { nil }
    sum { 1.5 }
    percentage { 1 }
    type { nil }
    number_of_month { 1 }
    static_return_value { 1 }
  end
end
