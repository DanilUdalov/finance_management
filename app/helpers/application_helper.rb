module ApplicationHelper
  def deptorable_name(debtor)
    if debtor.debtorable_type == 'User'
      debtor.debtorable.full_name
    else
      debtor.debtorable.name
    end
  end

  def lenderable_name(lender)
    if lender.lenderable_type == 'User'
      lender.lenderable.full_name
    else
      lender.lenderable.name
    end
  end
end
