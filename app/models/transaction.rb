class Transaction < ApplicationRecord
  belongs_to :debtor
  belongs_to :lender
end
