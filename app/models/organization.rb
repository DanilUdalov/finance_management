class Organization < ApplicationRecord
  has_many :debtors, as: :debtorable
  has_many :lenders, as: :lenderable
end
