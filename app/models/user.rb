class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  enum role: %i[user moderator admin]

  has_many :debtors, as: :debtorable
  has_many :lenders, as: :lenderable

  def full_name
    "#{first_name} #{last_name}"
  end

  def name
    "#{first_name} #{last_name}"
  end
end
