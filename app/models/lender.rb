class Lender < ApplicationRecord
  belongs_to :lenderable, polymorphic: true
  has_many :transactions
end
