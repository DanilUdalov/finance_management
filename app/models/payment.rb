class Payment < ApplicationRecord
  PAYMENT_STATE = ['pending', 'paid']
  belongs_to :debtor
end
