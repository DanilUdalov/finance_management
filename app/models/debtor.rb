class Debtor < ApplicationRecord
  belongs_to :debtorable, polymorphic: true
  has_many :transactions
  has_many :loans
  has_many :payments

  # validate :debtorable_exists?

  # private

  # def debtorable_exists?
  #   if self.class.where(debtorable_type: debtorable_type, debtorable_id: debtorable_id).any?
  #     errors.add(:base, 'Debtor exists for that user or organization')
  #   end
  # end
end
