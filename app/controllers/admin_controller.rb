class AdminController < ApplicationController
  layout 'application'
  before_action :require_admin

  private

  def require_admin
    unless current_user.try(:admin?)
      flash[:alert] = 'That panel is available only for admin!'
      redirect_to root_path
    end
  end
end
