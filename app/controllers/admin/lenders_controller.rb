class Admin::LendersController < AdminController
  before_action :find_debtor, only: :destroy

  def index
    @lenders = Lender.all
  end

  def create
    @lender = Lender.new(lender_params)
    if @lender.save
      flash[:notice] = "Debtor #{@lender.lenderable_id} was created"
      redirect_to admin_lenders_path
    else
      flash[:alert] = @lender.errors.full_messages.first
      redirect_back(fallback_location: admin_lenders_path)
    end
  end

  def destroy
    @lender.destroy
    flash[:alert] = "Debtor ##{@lender.id} has been destroyed!"
    redirect_back(fallback_location: admin_lenders_path)
  end

  private

  def find_lender
    @lender = Lender.find(params[:id])
  end

  def lender_params
    params.permit(:lenderable_id, :lenderable_type)
  end
end
