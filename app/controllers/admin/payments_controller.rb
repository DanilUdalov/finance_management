class Admin::PaymentsController < AdminController
  before_action :find_payment, except: [:new, :create, :index]
  before_action :debtors_list, only: [:new, :edit, :update]

  def index
    @payments = Payment.all
  end

  def new
    @payment = Payment.new
  end

  def create
    @payment = Payment.create(payment_params)
    if @payment.errors.present?
      render :new
    else
      flash[:notice] = "Payment #{@payment.id} has been created!"
      redirect_to admin_payments_path
    end
  end

  def update
    if @payment.update(payment_params)
      flash[:notice] = "Payment #{@payment.id} has been updated!"
      redirect_to admin_payments_path
    else
      render :edit
    end
  end

  def destroy
    @payment.destroy
    flash[:alert] = "Payment #{@payment.email} has been destroyed!"
    redirect_back(fallback_location: admin_payments_path)
  end

  private

  def debtors_list
    @debtors = Debtor.all
  end

  def find_payment
    @payment = Payment.find(params[:id])
  end

  private

  def payment_params
    params.require(:payment).permit(:sum, :description, :debtor_id, :state, :paid_at)
  end
end
