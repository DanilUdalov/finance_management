class Admin::OrganizationsController < AdminController
  before_action :find_organization, except: [:new, :create, :index]

  def index
    @organizations = Organization.order(:id)
  end

  def new
    @organization = Organization.new
  end

  def create
    @organization = Organization.create(organization_params)
    if @organization.errors.present?
      render :new
    else
      flash[:notice] = "Organization #{@organization.name} has been created!"
      redirect_to admin_organizations_path
    end
  end

  def update
    if @organization.update(organization_params)
      flash[:notice] = "Organization #{@organization.name} has been updated!"
      redirect_to admin_organizations_path
    else
      render :edit
    end
  end

  def destroy
    @organization.destroy
    flash[:alert] = "Organization #{@organization.name} has been destroyed!"
    redirect_back(fallback_location: admin_organizations_path)
  end

  private

  def find_organization
    @organization = Organization.find(params[:id])
  end

  def organization_params
    params.require(:organization).permit(:name, :description)
  end
end
