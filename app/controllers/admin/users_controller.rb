class Admin::UsersController < AdminController
  before_action :find_user, except: [:new, :create, :index]

  def index
    @users = User.order(:id)
  end

  def new
    @user = User.new
  end

  def create
    @user = User.create(user_params)
    if @user.errors.present?
      render :new
    else
      flash[:notice] = "User #{@user.email} has been created!"
      redirect_to admin_users_path
    end
  end

  def update
    if @user.update(user_params)
      flash[:notice] = "User #{@user.email} has been updated!"
      redirect_to admin_users_path
    else
      render :edit
    end
  end

  def destroy
    @user.destroy
    flash[:alert] = "User #{@user.email} has been destroyed!"
    redirect_back(fallback_location: admin_users_path)
  end

  private

  def find_user
    @user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit(:first_name, :last_name, :email, :password, :password_confirmation, :role)
  end
end
