class Admin::DebtorsController < AdminController
  before_action :find_debtor, only: :destroy

  def index
    @debtors = Debtor.all
  end

  def create
    @debtor = Debtor.new(debtor_params)
    if @debtor.save
      flash[:notice] = "Debtor #{@debtor.debtorable_id} was created"
      redirect_to admin_debtors_path
    else
      flash[:alert] = @debtor.errors.full_messages.first
      redirect_back(fallback_location: admin_debtors_path)
    end
  end

  def destroy
    @debtor.destroy
    flash[:alert] = "Debtor ##{@debtor.id} has been destroyed!"
    redirect_back(fallback_location: admin_debtors_path)
  end

  private

  def find_debtor
    @debtor = Debtor.find(params[:id])
  end

  def debtor_params
    params.permit(:debtorable_id, :debtorable_type)
  end
end
